import java.lang.annotation.*;
import java.lang.reflect.*;

public class AnnotationHandler {
	
	public void handle() {
		
		TestingClass o = new TestingClass();
		
		Class<?> c = o.getClass();
		
		try {
			Field[] f = c.getDeclaredFields();
			for (Field fld : f) {
				
				if (fld.isAnnotationPresent(NameAnno.class)) {
					String str;
					try {
						str = (String) fld.get(o);
						str = str.substring(0, 1).toUpperCase() + str.substring(1);
						System.out.println(str);
					} catch (IllegalArgumentException e) {
						e.printStackTrace();
					} catch (IllegalAccessException e) {
						e.printStackTrace();
					}	
				}
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		}
	}
}
